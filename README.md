# Digital Ocean: Alpine Linux

Script to install [Alpine Linux](https://alpinelinux.org/) on a [Digital Ocean](https://m.do.co/c/a0f96edad652) (referral link) droplet.

## Instructions

1. Create a new Debian (debian-10 x64) droplet on Digital Ocean
2. Download and run `digitalocean-alpine.sh` onto the droplet:

```sh
wget https://github.com/bontibon/digitalocean-alpine/raw/master/digitalocean-alpine.sh
chmod +x digitalocean-alpine.sh
./digitalocean-alpine.sh
```

## Status
2022-01-29: Repo is archived. Github fork trail suggests I'm not alone in wanting to continue using it. Test shows that digitalocean-alpine.sh tries to retrieve 'digitalocean' apk package from Tim Cooper's now-unavailable CDN. So, challenge is to see if the apk can still be built, made available and then tested working. CHANGELOG.md lists the code changes.

## License

MPL 2.0

## Author

Tim Cooper (<tim.cooper@layeh.com>)
